package vulkusb

import (
	"github.com/google/gousb"
	"vulklog"
)

type Usb struct {
	lg vulklog.Log
	err error

	//configs
	debug *int
	bufSize int
	cfgSetting int
	itfSetting int
	altIntfSetting int
	oEpSetting int
	iEpSetting int
	autoDetach bool

	//Vendor and Product IDs
	vid gousb.ID
	pid gousb.ID

	//interfaces
	ctx *gousb.Context
	dev *gousb.Device
	cfg *gousb.Config
	itf *gousb.Interface
	iep *gousb.InEndpoint
	oep *gousb.OutEndpoint
}

func (usb * Usb) SetDebug(debug int) {
	*usb.debug = debug
}

func (usb * Usb) SetBufSize(bufSize int) {
	usb.bufSize = bufSize
}

func (usb * Usb) SetCfgSetting(cfgSetting int) {
	usb.cfgSetting = cfgSetting
}

func (usb * Usb) SetItfSetting(itfSetting int) {
	usb.itfSetting = itfSetting
}

func (usb * Usb) SetAltIntfSetting(altIntfSetting int) {
	usb.altIntfSetting = altIntfSetting
}

func (usb * Usb) SetOEpSetting(oEpSetting int) {
	usb.oEpSetting = oEpSetting
}

func (usb * Usb) SetIEpSetting(iEpSetting int) {
	usb.iEpSetting = iEpSetting
}

func (usb * Usb) SetAutoDetach(autoDetach bool) {
	usb.autoDetach = autoDetach
}

func (usb * Usb) SetVid (vid gousb.ID) {
	usb.vid = vid
}

func (usb * Usb) SetPid (pid gousb.ID) {
	usb.pid = pid
}