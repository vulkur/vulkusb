package vulkusb

import (
	"github.com/google/gousb"
	"fmt"
)

func (usb * Usb) Init() {
	usb.lg.Init("usb0")
}

func (usb * Usb) Close() {
	usb.lg.Close()
    //TODO add checks for nil usb.* structs.
	usb.ctx.Close()
	usb.dev.Close()
	usb.cfg.Close()
	usb.itf.Close()
}
//Default config settings (besides vid and pid). Used for most basic USB devices.
func (usb * Usb) SetDefaultSettings() {
	usb.debug = new(int)
	*usb.debug = 0
	usb.bufSize = 16

	usb.cfgSetting = 1
	usb.itfSetting = 0
	usb.altIntfSetting = 0
	usb.oEpSetting = 1
	usb.iEpSetting = 1

	usb.autoDetach = true
}
//TODO add error checks for usb.*Settings
//Create connection. Requires all usb.*Settings to be set.
func (usb * Usb) CreateConnection() error {
	//Context
	usb.ctx = gousb.NewContext()
	usb.ctx.Debug(*usb.debug)

	//Device
	usb.dev, usb.err = usb.ctx.OpenDeviceWithVIDPID(usb.vid, usb.pid)
	if usb.err != nil {
		usb.lg.Logf("Error, unable to Open Device. %v", usb.err)
		return usb.err
	}
	usb.lg.Logf("Device: %s", usb.dev)
	usb.dev.SetAutoDetach(true)

	//Configuration
	usb.cfg, usb.err = usb.dev.Config(usb.cfgSetting)
	if usb.err != nil {
	    usb.lg.Logf("%s.Config(1): %v", usb.dev, usb.err)
	    return usb.err
	}
	usb.lg.Logf("Configuration: %s", usb.cfg.String())
	
	//Interface
	usb.itf, usb.err = usb.cfg.Interface(0, 0)
	if usb.err != nil {
	    usb.lg.Logf("%s.Interface(0, 0): %v", usb.cfg, usb.err)
	    return usb.err
	}
	usb.lg.Logf("Interface: %s", usb.itf.String())

	//In Endpoint
	if usb.iEpSetting > 0 {
		usb.iep, usb.err = usb.itf.InEndpoint(usb.iEpSetting)
		if usb.err != nil {
			usb.lg.Logf("%s.InEndpoint(%s): %v", usb.itf, usb.iEpSetting, usb.err)
			return usb.err
		}
		usb.lg.Logf("In Endpoint: %s", usb.iep.String())
	} else {
		usb.lg.Logf("In Endpoint: Not Opened")
	}

	//Out Endpoint
	if usb.oEpSetting > 0 {
		usb.oep, usb.err = usb.itf.OutEndpoint(usb.oEpSetting)
		if usb.err != nil {
			usb.lg.Logf("%s.OutEndpoint(%s): %v", usb.itf, usb.oEpSetting, usb.err)
			return usb.err
		}
		usb.lg.Logf("Out Endpoint: %s", usb.iep.String())
	} else {
		usb.lg.Logf("Out Endpoint: Not Opened")
	}
	return nil
}
//Read from created connection
func (usb * Usb) Read(length int) {
	// var sb strings.Builder
	var s string
	var value int8
	var size int
	var buf []byte = make([]byte, usb.bufSize)

	for i := 0; i < length; i++ {
		size, usb.err = usb.iep.Read(buf)
		if usb.err != nil {
			usb.lg.Logf("Failure to read from the device. %v", usb.err)
		}

		for j := 0; j < size; j++ {
			value = int8(buf[j])
			switch value {
				case 39:
					s = "0"
				case 30:
					s = "1"
				case 31:
					s = "2"
				case 32:
					s = "3"
				case 33:
					s = "4"
				case 34:
					s = "5"
				case 35:
					s = "6"
				case 36:
					s = "7"
				case 37:
					s = "8"
				case 38:
					s = "9"
				case 40:
					s = "\n"
				default:
					s = ""
			}
			fmt.Printf("%s",s)
		}
	}
}
